Package.describe({
  summary: 'simple spinner wrapper',
  version: '1.0.0',
  name: 'jsgoyette:spin'
});

Npm.depends({
  'spin.js': '2.0.2'
});

Package.onUse(function (api, where) {
  api.use([
    'templating',
    'underscore'
  ], 'client');

  api.addFiles([
    '.npm/package/node_modules/spin.js/spin.js',
    'lib/spinner.html',
    'lib/spinner.css',
    'lib/spinner.js'
  ], 'client');
});
